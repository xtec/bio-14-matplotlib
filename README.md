# Matplotlib

Aquest projecte correspon a l'activitat <https://xtec.dev/python/matplotlib/>

Pots baixar el projecte i treballar amb ell:

```sh
$ poetry install
$ poetry run fastapi dev app/main.py
```

Obre un navegador a <http://localhost:8000>

## Docker

Pots executar directament la imatge que està al registre de GitLab:

```sh
docker run --rm -d -p 80:80 registry.gitlab.com/xtec/python/matplotlib
```

Pots crear una imatge nova:

```sh
docker build --tag plot .
```

I executar la imatge que has creat:

```sh
docker run --rm -d -p 80:80 plot
```

