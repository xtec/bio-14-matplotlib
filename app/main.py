from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from io import BytesIO
import matplotlib.pyplot as plt
import base64

app = FastAPI()


@app.get("/", response_class=HTMLResponse)
def index():

    create_plot()

    # Save it to a temporary buffer.
    buf = BytesIO()
    plt.savefig(buf, format="png")
    plt.clf()
    buf.seek(0)

    # Encode image to Base64
    image = base64.b64encode(buf.getvalue()).decode("utf8")

    # Return HTML document with inline image
    return f"""
    <html>
        <head>
            <title>Plot</title>
        </head>
        <body>
            <img src="data:image/png;base64,{image}">
        </body>
    </html>
    """


def create_plot():
    # Create plot with matplotlib

    plt.title("Gràfic")
    plt.plot([10, 30, 25, 40, 10, 5, 10], linestyle="dashed")
